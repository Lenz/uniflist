#!/usr/bin/python
# coding: utf8

from distutils.core import setup

with open('README') as f:
    readme = f.read()

setup(name='uniflist',
      version='1.0',
      author='Lenz Furrer',
      author_email='lenz.furrer@uzh.ch',
      license='GPLv3',
      description='Python module for indexed, partial-match sequences',
      long_description=readme,
      py_modules=['uniflist'])
