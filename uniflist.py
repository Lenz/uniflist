#!/usr/bin/env python
# coding: utf8

# uniflist: Python module for indexed, partial-match sequences
# Copyright 2015 Lenz Furrer <lenz.furrer@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


'''
This module contains a single class: UnifList.

Usage example:
>>> from uniflist import UnifList
>>> _ = UnifList._              # wildcard as "_" in global namespace
>>> p = UnifList(3)             # new empty UnifList
>>> p.append('x', 'y', 'z')     # add data
>>> p.append('x', 'x', _)       # data with wildcard
>>> p.append('z', 'y', 'x')
>>> p
UnifList(3, [('x', 'y', 'z'), ('x', 'x', _), ('z', 'y', 'x')])
>>> list(p.get('x', _, 'z'))    # query with wildcards
[('x', 'y', 'z'), ('x', 'x', 'z')]
>>> list(p.get('x'))            # query with implicit wildcards
[('x', 'y', 'z'), ('x', 'x', _)]
>>> list(p.get(_, 'y'))         # explicit and implicit wildcards
[('x', 'y', 'z'), ('z', 'y', 'x')]
'''


from __future__ import division, unicode_literals, print_function

import itertools


__all__ = ('UnifList',)


class UnifList(object):
    '''
    UnifList: indexed sequence with partial look-up.

    UnifList mimicks the behaviour of a Prolog predicate.
    It is a container for hashable tuples of equal length.
    Entries can be queried with partial look-up.
    The order of the entries is preserved.
    Duplicates are not removed.

    Data and queries may feature wildcards, which match any
    single element of a data/query entry.
    The wildcard is represented by a special class attribute:
        UnifList._

    See the module docstring for a usage example.
    '''

    # Inline class definition: Create an object with a unique instance.
    _ = type(b'Wildcard', (object,), {'__repr__': lambda self: '_'})()

    def __init__(self, *args, **kwargs):
        '''
        UnifList(int) -> new empty UnifList with arity ``int``
        UnifList(iterable) -> new UnifList initialised from an
            iterable of equally sized entries
        UnifList(arity=int, entries=iterable) -> new UnifList
            initialised with explicit keyword arguments
        '''
        arity, entries = self._resolve_init_arg(*args, **kwargs)

        self.arity = arity
        self._entries = []
        self._index = tuple({} for _ in range(arity))
        self._wildcard = tuple(set() for _ in range(arity))

        for entry in entries:
            self._append(entry)

    @staticmethod
    def _resolve_init_arg(arity=None, entries=()):
        '''
        Prepare args for different constructor signatures.
        '''
        # Explicit arity.
        if isinstance(arity, int):
            return arity, entries

        # Implicit arity: determine from first entry.
        # The entries might be in "entries" (if given as kwarg) or in "arity"
        # (if given as sole positional argument).
        # Create two shallow iterator copies:
        # Peek at the first entry in one copy, while keeping an iterator over
        # the full "entries" iterable in the other.
        try:
            entries, peek_copy = itertools.tee(entries or arity)
            first = peek_copy.next()
            del peek_copy
        except (StopIteration, TypeError):
            raise ValueError('need either explicit arity (int), '
                             'or a non-empty iterable of entry data')
        return len(first), entries

    def append(self, *keys):
        '''
        Add an entry to the collection.

        Wildcards using UnifList._ are allowed.

        Args:
            keys (hashables): elements of an entry

        Returns:
            None
        '''
        self._append(keys)

    def _append(self, keys):
        '''
        Add an entry to the collection without argument unpacking.
        '''
        if len(keys) != self.arity:
            raise ValueError('wrong argument number: must be %d, not %d' %
                             (self.arity, len(keys)))

        n_index = len(self._entries)
        for i, k in enumerate(keys):
            if k is self._:
                self._wildcard[i].add(n_index)
            else:
                try:
                    self._index[i][k].add(n_index)
                except KeyError:
                    self._index[i][k] = set([n_index])
        self._entries.append(tuple(keys))

    def get(self, *keys):
        '''
        Retrieve all entries that match ``keys``.

        Wildcards using UnifList._ are allowed.
        If ``keys`` is too short, it is right-padded with
        wildcards.
        The retrieved keys are unified with ``keys``,
        ie. wildcards are interpolated on both sides.

        Args:
            keys (hashables): query elements

        Returns:
            an iterator over tuples: all matching entries
        '''
        if len(keys) < self.arity:
            keys += (self._,) * (self.arity - len(keys))
        for i in self._match(keys):
            yield self.unify(self._entries[i], keys)

    def count(self, *keys):
        '''
        Count the number of matches for ``keys``.

        Args:
            keys (hashables): query elements

        Returns:
            int: the number of matches
        '''
        return len(self._match(keys))

    @classmethod
    def unify(cls, left, right):
        '''
        Replace wildcards in ``left`` with elements from ``right``.

        Args:
            left (sequence): main entry
            right (sequence): back-off data

        Returns:
            tuple: unified entry
        '''
        return tuple(l if l is not cls._ else right[i]
                     for i, l in enumerate(left))

    def _match(self, keys):
        '''
        Match all entries against ``keys``.

        Args:
            keys (sequence): elements

        Returns:
            list or xrange: indexes of all matching lines,
                            in order
        '''
        matches = None
        for i, k in enumerate(keys):
            if k is self._:
                continue
            s = self._wildcard[i].union(self._index[i].get(k, ()))
            try:
                matches.intersection_update(s)
            except AttributeError:
                matches = s
        if matches is None:
            return xrange(len(self._entries))
        return sorted(matches)

    def __repr__(self):
        return '%s(%d, %r)' % (self.__class__.__name__,
                               self.arity, self._entries)

    def __iter__(self):
        return iter(self._entries)

    def __reversed__(self):
        return reversed(self._entries)

    def __len__(self):
        return len(self._entries)

    def __eq__(self, obj):
        if isinstance(obj, self.__class__):
            return self is obj or self._entries == obj._entries
        return False

    def __ne__(self, obj):
        return not self == obj

    def __contains__(self, keys):
        return bool(self._match(keys))
